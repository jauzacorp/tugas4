package id.ac.ub.papb.recycler1;

public class Mahasiswa {
    public String nim;
    public String nama;

    public Mahasiswa(String nim, String nama){
        this.nim= nim;
        this.nama = nama;
    }
}
