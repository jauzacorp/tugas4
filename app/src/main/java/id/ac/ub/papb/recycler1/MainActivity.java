package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    public static String TAG = "RV1";
    private ArrayList<Mahasiswa> data = new ArrayList<>();
    private MahasiswaAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rv1 = findViewById(R.id.rv1);
//        ArrayList<Mahasiswa> data = getData();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));

        Button btnsimpan = findViewById(R.id.bt1);
        btnsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String namaBaru = ((EditText) findViewById(R.id.editTextTextPersonName2)).getText().toString();
                String nimBaru = ((EditText) findViewById(R.id.etNim)).getText().toString();

                if(!namaBaru.isEmpty() && !nimBaru.isEmpty()){
                    Mahasiswa mahasiswaBaru = new Mahasiswa(nimBaru, namaBaru);
                    data.add(mahasiswaBaru);
                    adapter.notifyDataSetChanged();

                    ((EditText) findViewById(R.id.editTextTextPersonName2)).setText("");
                    ((EditText) findViewById(R.id.etNim)).setText("");
                }

            }
        });

//        SharedPreferences sharedpref = this.getPreferences(MainActivity.MODE_PRIVATE);
//        SharedPreferences.Editor editor = this.getPreferences(MainActivity.MODE_PRIVATE);

//    public ArrayList getData() {
//            ArrayList<Mahasiswa> data = new ArrayList<>();
//            List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
//            List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
//
//
//    }

//    public void showData(){
//        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
//        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
//        for (int i = 0; i < nim.size(); i++){
//            Mahasiswa mhs = new Mahasiswa();
//            mhs.nim = nim.get(i);
//            mhs.nama = nama.get(i);
//            Log.d(TAG, "getData" + mhs.nim);
//            getData().get(i);
//        }
    }
}